export const styles = {
    header: {
        backgroundColor: '#f2f2f2',
        padding: '10px',
        display:'flex',
        alignItems:'center',
        justifyContent: 'space-between',
    },
    headerList: {
        listStyleType: 'none',
        display: 'flex',
        alignItems: 'center',
    },
    addBtn: {

    },
    headerBtn: {
        height: 34,
        marginRight: '10px',
        cursor: 'pointer',
        color: '#333',
        padding: '0 16px',
        borderRadius: '6px',
        backgroundColor: 'rgba(30, 49, 80, 0.05)',

        gap: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        border: '1px solid transparent',

        // '&:after': {
        // '&:before': {
        '&:hover': {
            backgroundColor: 'rgba(0, 177, 255, 0.1)',
            border: '1px solid #00B1FF',
        },
        '&.active': {
            backgroundColor: '#1e3150',
            color: '#fff',
            border: '1px solid transparent',
        },
        // '& .active': {},
    },
    headerBtnAdd: {
        marginLeft: 'auto',
    },
    headerBtnLink: {
        // textDecoration: 'none!important',
        color: 'red!important',
        // padding: '5px 10px',
        // borderRadius: '5px',
        // backgroundColor: '#ccc',
    },
    headerBtnLinkHover: {
        backgroundColor: '#999',
        color: '#fff',
        cursor: 'pointer',
    },
    headerBtnAddLink: {
        backgroundColor: '#f44336',
        color: '#fff',
        padding: '5px 10px',
        borderRadius: '5px',
        cursor: 'pointer',
    },
    headerBtnAddLinkHover: {
        backgroundColor: '#d32f2f',
    },
};