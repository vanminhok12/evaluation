import React from "react";
import { routerPush } from "../../../../utils/RouterUtils";
import withStyles from "@material-ui/core/styles/withStyles";
import { styles } from "./styles";
import TemporaryDrawer from "../form";
class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            tags:
                [
                    {key: 'list',value:'Tất cả'},
                    {key:'contact', value: 'Khách hàng'},
                    {key: 'call', value: 'Lịch sử cuoc gọi'},
                    {key: 'agent', value: 'Nhân viên'},
                    {key: 'ticket', value: 'Phiếu ghi'},
                    {key: 'multi_channel', value: 'Đa kênh'},
                ],
            activeTag:null,
        };
    };
    componentDidMount() {
        const scope = window.location.pathname.split('/').pop();
        this.setState({ activeTag: scope });
    }

    componentDidUpdate(prevProps, prevState) {
        const { activeTag } = this.state;
        const { onActiveTagChange } = this.props;if (prevState.activeTag !== activeTag) {
            onActiveTagChange(activeTag);
            routerPush(`/evaluation/${activeTag}`);
        }
    };
    handleClick = (key) => {
        this.setState({ activeTag: key });
    }

    render() {
        const { classes } = this.props;
        const { tags, activeTag } = this.state;

        return (
            <div className={classes.header}>
                <ul className={classes.headerList}>
                    {tags.map((tag) => {
                        const { key, value } = tag;
                        const isActive = key === activeTag;

                        return (
                            <div
                                key={key}
                                className={`${classes.headerBtn} ${isActive ? 'active' : ''}`}
                                onClick={() => this.handleClick(key)}
                            >
                                <span className={isActive ? 'active' : ''}>{value}</span>
                            </div>
                        );
                    })}
                </ul>
                <div className={classes.addBtn}>
                    <TemporaryDrawer />
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Header);