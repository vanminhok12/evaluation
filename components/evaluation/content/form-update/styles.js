const styles = {
    list: {
        width: 550,
    },
    fullList: {
        width: 'auto',
    },
    form: {
        padding: '16px',
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-start',
        marginTop: '21rem',
    },
    button: {
        width: 115,
        height: 48,
        marginRight: '8px',
        backgroundColor: 'rgb(86, 204, 110)',
        '&:hover':{
            backgroundColor: 'rgb(86, 204, 110)',
        },
    },
    addButton: {
        position: 'absolute',
        top: '0',
        left: '0',
    },
    closeBtn: {
        marginRight: '8px',
        border: '1px solid rgba(0, 177, 255, 0.1)',
        backgroundColor: '#f5f6fa',
        color: '#333',
        '&:hover':{
            backgroundColor: '#f5f6fa',
        },
    },
    switchContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: '8px',
    },
    checkboxContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: '8px',
    },
    checkboxLabel: {
        marginLeft: '8px',
    },
    switch: {
        display:'flex',
        alignItems:'center',
        justifyContent:'center',
    },
    header: {
        display:'flex',
        justifyContent:'space-between',
    },
    content: {
        marginTop: '3rem',
    },
    option: {
        display: 'flex',
    },
    input: {
        width: '490px',
        height: '43px',
        '& input': {
            color: 'inherit',
            '&:hover': {
                backgroundColor: 'lightblue',
            },
            '&:focus': {
                backgroundColor: 'lightblue',
            },
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'lightgray',
            },
            '&:hover fieldset': {
                borderColor: 'lightblue',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'lightblue',
            },
        },
        '& .MuiInputLabel-root': {
            fontSize: '14px',
        },
    },
};
export default styles;