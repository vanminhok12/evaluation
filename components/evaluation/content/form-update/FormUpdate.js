import React, { Component, useRef } from 'react';
import { compose } from "redux";
import { connect } from "react-redux";
import clsx from 'clsx';

import EvaluationActions from "../../../../reduxs/EvaluationRedux";

import { withStyles } from '@material-ui/core/styles';
import styles from "./styles";

import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Checkbox } from "@material-ui/core";

const classify = { evaluation: 'evaluations' };

class FormUpdate extends Component {
    constructor(props) {
        super(props);
        this.nameRef = React.createRef();
        this.state = {
            right: false,
            scopes: [],
        };
    }

    toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        this.setState({ [anchor]: open });
    };

    handleClose = (anchor) => {
        // this.setState({ [anchor]: false });
        this.props.onCloseForm();
    };

    handleChange = (event) => {
        const { checked, name } = event.target;
        this.setState((prevState) => {
            if (checked) {
                return {
                    scopes: [...prevState.scopes, name],
                };
            } else {
                return {
                    scopes: prevState.scopes.filter((scope) => scope !== name),
                };
            }
        });
    };


    handleSubmit = (event) => {
        event.preventDefault();
        const {evaluation} = this.props;
        console.log('  handleSubmit ')
        const { scopes } = this.state;
        const data = {
            type: 'update',
            evaluationId: evaluation._id,

            name: this.nameRef.current.value,
            scopes: ['call'],
            contact_ids: [],
            contact_category_ids: [],
        };

        this.props.crudEvaluation(classify.evaluation, data)
    };

    render() {
        const { classes, onShow, evaluation } = this.props;
        const anchor = 'right';
        const { right,  } = this.state;
        const { created_date, name, _id, scopes } = evaluation;
        let existingScopes = scopes? scopes: [];
        return (
            <div>
                <React.Fragment key={anchor}>
                    <Drawer
                        anchor='right'
                        open={onShow}
                        onClose={() => this.props.onCloseForm()}
                        BackdropProps={{
                            style: {
                                backdropFilter: 'blur(0px)',
                                backgroundColor: 'rgba(255, 255, 255, 0.3)',
                            },
                        }}
                    >
                        <div
                            className={clsx(classes.list, {
                                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
                            })}
                            role="presentation"
                            onKeyDown={this.toggleDrawer(anchor, false)}
                        >
                            <div className={classes.form}>
                                <div className={classes.header}>
                                    <h2>Chỉnh sửa</h2>
                                </div>
                                <form onSubmit={this.handleSubmit}>
                                    <TextField
                                        label="Name"
                                        inputRef={this.nameRef}
                                        onClick={(e) => e.stopPropagation()}
                                        onKeyDown={(e) => e.stopPropagation()}
                                        className={classes.input}
                                        defaultValue={name}
                                        variant="outlined"
                                        required
                                    />

                                    <div className={classes.content}>
                                        <label>Sử dụng cho</label><br />
                                        <div className={classes.option}>
                                            {existingScopes.map((scope) => (
                                                <React.Fragment key={scope}>
                                                    <Checkbox
                                                        defaultChecked
                                                        color="primary"
                                                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                                                        name={scope}
                                                        onClick={this.handleChange}
                                                    />
                                                    <p>{scope}</p>
                                                </React.Fragment>
                                            ))}
                                        </div>
                                    </div>
                                    <br />
                                    <div className={classes.buttonContainer}>
                                        <Button variant="contained" color="primary" className={classes.button} type='submit'>
                                            Cập nhật
                                        </Button>
                                        <Button variant="contained" color="secondary" className={classes.closeBtn} onClick={() => this.props.onCloseForm()}>
                                            Đóng
                                        </Button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </Drawer>
                </React.Fragment>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { accessToken } = state.session;
    return {
        // session
        accessToken,
        // evaluation
        evaluationFetching: state.evaluation.fetching,
        evaluationContent: state.evaluation.content,
    };
};

const mapDispatchToProps = dispatch => ({
    // evaluation
    crudEvaluation: (classify, params) => dispatch(EvaluationActions.crudEvaluationRequest(classify, params)),
});

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(FormUpdate);
