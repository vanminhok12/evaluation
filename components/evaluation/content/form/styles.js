const styles = {
    list: {
        width: 550,
    },
    customDrawerPaper: {
        marginLeft: '28rem',
        borderRadius: '30rem',
        backgroundColor: '#333',
    },
    fullList: {
        width: 'auto',
    },
    form: {
        padding: '16px',
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-start',
        marginTop: '21rem',
    },
    button: {
        marginRight: '8px',
        width: 115,
        height: 48,
        backgroundColor: 'rgb(86, 204, 110)',
        '&:hover':{
            backgroundColor: 'rgb(86, 204, 110)',
        },
    },
    closeBtn: {
        marginRight: '8px',
        border: '1px solid rgba(0, 177, 255, 0.1)',
        backgroundColor: '#f5f6fa',
        color: '#333',
        '&:hover':{
            backgroundColor: '#f5f6fa',
        },
    },
    addButton: {
        position: 'absolute',
        top: '0',
        left: '0',
    },
    showBtn: {
      backgroundColor: 'rgb(86, 204, 110)',
        '&:hover':{
            backgroundColor: 'rgb(86, 204, 110)!important',
        },
    },
    switchContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: '8px',
    },
    checkboxContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: '8px',

    },
    checkboxLabel: {
        marginLeft: '8px',
    },
    switch: {
        display:'flex',
        alignItems:'center',
        justifyContent:'center',
    },
    header: {
        display:'flex',
        justifyContent:'space-between',
    },
    content: {
        marginTop: '3rem',
    },
    option: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    optionContent:{
        display:'flex',
    },
    input: {
        width: '490px',
        height: '43px',
        '& input': {
            color:  '#333',
            '&:hover': {
                // backgroundColor:  'rgba(0, 177, 255, 0.1)',
            },
            '&:focus': {
                backgroundColor: 'rgba(0, 177, 255, 0.1)',
            },
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'rgba(0, 177, 255, 0.1)',
            },
            '&:hover fieldset': {
                    backgroundColor: 'rgba(0, 177, 255, 0.1)',
                    border:  'rgba(0, 177, 255, 0.1)',
            },
            '&.Mui-focused fieldset': {
                borderColor:  'rgba(0, 177, 255, 0.1)',
            },
        },
        '& .MuiInputLabel-root': {
            fontSize: '14px',
        },
    },
};
export default styles;