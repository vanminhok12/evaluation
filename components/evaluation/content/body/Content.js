import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import EvaluationActions from "../../../../reduxs/EvaluationRedux";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import { ImageViewer } from "../../../common";
import Header from "../header";
import FormUpdate from "../form-update";
import deleteIcon from "../../../../assets/icons/common/ic_delete.png";

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            classify: {
                list: 'evaluations',
            },
            evaluations: [],
            showDeleteAlert: false,
            showUpdateForm: false,
            activeTag: null,
        };
    }

    componentDidMount() {
        const { classify } = this.state;
        const params = {
            page: 1,
            size: 10,
            body: {
                "scopes": [],
                "keyword": "",
                "sort": {
                    "field": "created_date",
                    "isAsc": false
                },
            },
        };
        this.props.getEvaluations(classify.list, params);
        const scope = window.location.pathname.split('/').pop();
        this.setState({ activeTag: scope });
    }

    componentDidUpdate(prevProps, prevState) {
        const { activeTag } = this.state;

        if (prevState.activeTag !== activeTag) {
            const { classify } = this.state;
            const params = {
                page: 1,
                size: 10,
                body: {
                    "scopes": [],
                    "keyword": "",
                    "sort": {
                        "field": "created_date",
                        "isAsc": false
                    },
                },
            };
            this.props.getEvaluations(classify.list, params);
        }
    }

    showAlert = (id) => {
        this.setState({ showDeleteAlert: true, deleteEvaluationId: id });
    };

    formatDate = (created_date) => {
        const dateObject = new Date(created_date);
        const formattedDate = `${dateObject.getDate()}/${dateObject.getMonth() + 1}/${dateObject.getFullYear()}`;
        const formattedTime = `${dateObject.getHours()}:${dateObject.getMinutes()}`;
        return { formattedDate, formattedTime };
    }

    handleCloseDeleteAlert = () => {
        this.setState({ showDeleteAlert: false, deleteEvaluationId: null });
    };

    handleDelete = () => {
        const { classify, deleteEvaluationId } = this.state;
        const params = { evaluationId: deleteEvaluationId, type: 'delete' };
        this.props.crudEvaluation(classify.list, params);
        this.handleCloseDeleteAlert();
    };

    handleToggleUpdateForm = (evaluation) => {
        this.setState((prevState) => ({
            evaluation,
            showUpdateForm: !prevState.showUpdateForm,
        }));
    };

    _renderDialogConfirm = (_id) => {
        const { showDeleteAlert } = this.state;
        return (
            <Dialog
                open={showDeleteAlert}
                onClose={this.handleCloseDeleteAlert}
                BackdropProps={{
                    style: {
                        backdropFilter: 'blur(0px)',
                        backgroundColor: 'rgba(255, 255, 255, 0.3)',
                    },
                }}
            >
                <DialogTitle id="alert-dialog-title">{"Do you want to delete?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description"></DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCloseDeleteAlert} color="primary">
                        No
                    </Button>
                    <Button onClick={() => this.handleDelete(_id)} color="primary">
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    handleActiveTagChange = (activeTag) => {
        this.setState({ activeTag });
    }

    render() {
        const { classify, showUpdateForm, evaluation } = this.state;
        const { classes, evaluationContent, evaluationFetching } = this.props;
        const scope = window.location.pathname.split('/').pop();

        return (
            <>
                <Header onActiveTagChange={this.handleActiveTagChange} />
                <div className={classes.content}>
                    <table className={`${classes.customTable} responsive-table`}>
                        <thead>
                        <tr>
                            <th className={classes.customTh}>#</th>
                            <th className={classes.customTh}>Bộ đánh giá</th>
                            <th className={classes.customTh}>Phân loại</th>
                            <th className={classes.customTh}>Phân quyền</th>
                            <th className={classes.customTh}>Ngày tạo</th>
                            <th className={classes.customTh}>--</th>
                        </tr>
                        </thead>
                        <tbody>
                        {evaluationFetching[classify.list]
                            ? <div>loading...</div>
                            : evaluationContent[classify.list]?.map((evaluation, index) => {
                                const { created_date, name, scopes, _id } = evaluation;
                                const { formattedDate, formattedTime } = this.formatDate(created_date);

                                // Check scope and filter evaluations
                                const shouldRender = scope === "list" || (scopes && scopes.includes(scope));

                                // If it should not render, skip this evaluation
                                if (!shouldRender) return;
                                return (
                                    <tr key={_id} className={classes.customRow} onClick={() => this.handleToggleUpdateForm(evaluation)}>
                                        <th className={classes.customTd}>{index + 1}</th>
                                        <td className={classes.customTd}>
                                            {name}
                                        </td>
                                        <td className={classes.customTd}>|</td>
                                        <td className={classes.customTd}>
                                            {scopes.join(" / ")}
                                        </td>
                                        <td className={classes.customTd}>{`${formattedTime} ${formattedDate}`}</td>
                                        <td className={classes.customTd}>
                                            <ImageViewer
                                                src={deleteIcon}
                                                size={20} className={classes.deleteIcon}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    this.showAlert(_id);
                                                }}
                                            />
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
                {this._renderDialogConfirm()}
                {showUpdateForm && (
                    <FormUpdate
                        onShow={showUpdateForm}
                        evaluation={evaluation}
                        onCloseForm={this.handleToggleUpdateForm} // Pass the close form handler as a prop
                    />
                )}
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        // evaluation
        evaluationFetching: state.evaluation.fetching,
        evaluationContent: state.evaluation.content,
    };
}

const mapDispatchToProps = dispatch => ({
    // evaluation
    getEvaluations: (classify, params) => dispatch(EvaluationActions.getEvaluationsRequest(classify, params)),
    crudEvaluation: (classify, params) => dispatch(EvaluationActions.crudEvaluationRequest(classify, params)),
});

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Content);
