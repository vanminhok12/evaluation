import { all, takeLatest} from 'redux-saga/effects';

/* ------------- START: Types ------------- */

import { RoleTypes } from '../reduxs/RoleRedux';

import { UserTypes } from '../reduxs/UserRedux';
import { EvaluationTypes } from "../reduxs/EvaluationRedux";

/* ------------- END: Types ------------- */

/* ------------- START: Sagas ------------- */

import { getRoles } from './RoleSaga';

import { userLogin, getUserInfo } from './UserSaga';
import { getEvaluations, crudEvaluation } from "./EvaluationSaga";

/* ------------- END: Sagas ------------- */

/* ------------- START: Connect Types To Sagas ------------- */
export default function* root() {
  yield all([

    // user
    takeLatest(UserTypes.USER_LOGIN_REQUEST, userLogin),
    takeLatest(UserTypes.GET_USER_INFO_REQUEST, getUserInfo),

    // role
    takeLatest(RoleTypes.GET_ROLES_REQUEST, getRoles),

    // evaluation
    takeLatest(EvaluationTypes.GET_EVALUATIONS_REQUEST, getEvaluations),

    takeLatest(EvaluationTypes.CRUD_EVALUATION_REQUEST, crudEvaluation),
  ]);
}
/* ------------- END: Connect Types To Sagas ------------- */