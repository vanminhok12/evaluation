import { call, put, delay } from 'redux-saga/effects';
import EvaluationActions, {crudEvaluationSuccess} from '../reduxs/EvaluationRedux';
import { getTimestamp, getDelayTime } from '../utils/DateUtils';
import { validateResp, getErrorMsg } from '../utils/StringUtils';

import EvaluationAPIs from "../services/APIs/EvaluationAPIs";

const api = new EvaluationAPIs();

export function* getEvaluations(action) {
    const { classify, params } = action;
    const startReqAt = getTimestamp();
    try {
        let resp = yield call(api.getEvaluations, params);
        yield delay(getDelayTime(startReqAt, 'ms', 500));
        if (validateResp(resp)) {
            yield put(EvaluationActions.getEvaluationsSuccess(classify, resp.payload));
        } else throw resp;
    } catch (error) {
        yield put(EvaluationActions.evaluationCommonFailure(classify, getErrorMsg(error)));
    };
};

export function* crudEvaluation({ classify,payload, params }) {
    try {
        const resp = yield call(api.crudEvaluation, params);
        if (validateResp(resp)) {
            yield put(EvaluationActions.crudEvaluationSuccess(classify, payload, params));
        } else {
            throw resp;
        }
    } catch (error) {
        yield put(EvaluationActions.evaluationCommonFailure(classify, getErrorMsg(error)));
    }
}

