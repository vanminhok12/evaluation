import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- START: Types and Action Creators ------------- */
const { Types, Creators } = createActions({

    evaluationCommonSuccess: ['classify', 'payload'],
    evaluationCommonFailure: ['classify', 'error'],

    getEvaluationsRequest: ['classify', 'params'], // classify: evaluation
    getEvaluationsSuccess: ['classify', 'payload'],


    crudEvaluationRequest: ['classify', 'params'],
    crudEvaluationSuccess: ['classify', 'payload', 'params'],

});
export const EvaluationTypes = Types;
export default Creators;
/* ------------- END: Types and Action Creators ------------- */

/* ------------- START: Initial State ------------- */
export const INITIAL_STATE = Immutable({
    error: {},
    fetching: {},
    content: {},
    pagination: {},
    // content: {
    //     list: [],
    //     detail: {},
    // },
});
/* ------------- END: Initial State ------------- */

/* ------------- START: common ------------- */
export const evaluationCommonSuccess = (state, { classify, payload }) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        content: { ...state.content, [classify]: payload },
    });
};

export const evaluationCommonFailure = (state, { classify, error }) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        error: { ...state.error, [classify]: error },
    });
};
/* ------------- END: common ------------- */

/* ------------- START: getEvaluations ------------- */
export const getEvaluationsRequest = (state, { classify }) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: true },
        error: { ...state.error, [classify]: null },
        content: { ...state.content, [classify]: [] },
    });
};

export const getEvaluationsSuccess = (state, { classify, payload }) => {
    const { items, ...rest } = payload;
    return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        content: { ...state.content, [classify]: items || [] },
        pagination: { ...state.pagination, [classify]: rest },
    });
};
/* ------------- END: getEvaluations ------------- */



export const crudEvaluationRequest = (state, { classify}) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: true },
        error: { ...state.error, [classify]: null },
    });
};
export const crudEvaluationSuccess = (state, { classify,payload, params }) => {
    const {type, ...data} = params;
    const evaluations = state.content[classify] || [];
    let updatedEvaluations = [];

    switch (type) {
        case 'add':
            updatedEvaluations = [...evaluations, data];
            break;
        case 'update':
            updatedEvaluations = evaluations.map((evaluation) =>
                evaluation._id === data.evaluationId ? { ...evaluation, ...data } : evaluation
            );
            break;
        case 'delete':
            updatedEvaluations = evaluations.filter(
                (evaluation) => evaluation._id !== data.evaluationId
            );
            break;
        default:
            updatedEvaluations = evaluations;
            break;
    }

    return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        content: {
            ...state.content,
            [classify]: updatedEvaluations,
        },
    });
};


/* ------------- END: deleteEvaluation ------------- */

/* ------------- START: Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {

    [Types.EVALUATION_COMMON_SUCCESS]: evaluationCommonSuccess,
    [Types.EVALUATION_COMMON_FAILURE]: evaluationCommonFailure,

    [Types.GET_EVALUATIONS_REQUEST]: getEvaluationsRequest,
    [Types.GET_EVALUATIONS_SUCCESS]: getEvaluationsSuccess,

    [Types.CRUD_EVALUATION_REQUEST]: crudEvaluationRequest,
    [Types.CRUD_EVALUATION_SUCCESS]: crudEvaluationSuccess,

});
/* ------------- END: Hookup Reducers To Types ------------- */
