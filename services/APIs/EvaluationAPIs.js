import { doRequest } from '../../utils/CoreUtils';
import { errorHandler } from '../../utils/StringUtils';

import { contact_domain } from '../../constants/Domain';

export default class EvaluationAPIs {

    async getEvaluations({ page, size, body } = {}) {
        try {
            let url = `${contact_domain}evaluation_criteria/search?page=${page}&size=${size}`;
            return await doRequest('post', url, { body });
        } catch (error) {
            throw errorHandler(error);
        };
    };

    async crudEvaluation({ type, evaluationId, ...body } = {}) {
        // type: add | delete | update
        try {
            let url = `${contact_domain}evaluation_criteria/${type}`;
            if (type !== 'add') url += `/${evaluationId}`;
            return await doRequest('post', url, { body });
        } catch (error) {
            throw errorHandler(error);
        }
    }

}