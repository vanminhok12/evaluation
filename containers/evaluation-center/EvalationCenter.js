import React, { Component } from 'react'
import { getRouterQuery, replaceRoute } from '../../utils/RouterUtils';

class EvalationCenter extends Component {

    constructor(props) {
        super(props);
        const { pageId } = getRouterQuery();
        this.state = {
            pageId,
            tabPages: [
                { id: 'list', name: 'Tất cả' },
                { id: 'contact', name: 'Khách hàng' },
                { id: 'call', name: 'Lịch sử cuộc gọi' },
            ],
        }
    }

    handleChangePage = (id) => {
        replaceRoute(`/evaluation/${id}`);
        this.setState({ pageId: id });
    }

    render() {
        const { pageId, tabPages } = this.state;
        return (
            <div>
                <div>
                    <ul>
                        {tabPages.map((tab, index) => {
                            const { id, name } = tab;
                            return (
                                <li
                                    key={id}
                                    style={{
                                        color: pageId === id ? 'red' : 'black'
                                    }}
                                    onClick={() => this.handleChangePage(id)}
                                >
                                    <span>{name}</span>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        )
    }
}

export default EvalationCenter